"""shopping URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from django.contrib.staticfiles import views
import settings
from django.views.static import serve
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
admin.autodiscover()
urlpatterns = [

    url(r'^admin/', admin.site.urls),
    
    
]
urlpatterns += staticfiles_urlpatterns()
# if settings.DEBUG:
#     # urlpatterns += [
#     #     url(r'^static/(?P<path>.*)$', views.serve),
#     # ]
#     urlpatterns += [url(r'^media/(?P<path>.*)$', views.serve)]

if settings.DEBUG:
    urlpatterns += [
        url(r'^media/(?P<path>.*)$', serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
    ]
urlpatterns+=[url(r'^',include('users.urls')),]
       