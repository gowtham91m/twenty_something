from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class Roles(models.Model):
    name = models.CharField("name",max_length=255)
    shortcode = models.CharField("shortcode",max_length=255)
    comments = models.CharField("comments",max_length=255,blank=True,null=True)

    def __unicode__(self):

        return "%s %s" %(self.name,self.shortcode)


class Profile(models.Model):
    user = models.ForeignKey(User)
    role = models.ForeignKey(Roles,blank=True,null=True)
    phone = models.CharField("Phone Number",max_length=255)
    address = models.TextField(blank=True,null=True)
    city = models.TextField(blank=True,null=True)
    state = models.TextField(blank=True,null=True)
    pincode = models.IntegerField(blank=True,null=True)


    def __unicode__(self):
        return "%s %s %s %s" %(self.user,self.role,self.phone,self.city)