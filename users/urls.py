from django.conf.urls import url
from users import views


urlpatterns = [
    url(r'^login/$', views.login),
    url(r'^logout/$', views.logout),
    url(r'^returns/$', views.returns),
    url(r'^offers/$', views.offers),
    url(r'^cart/$', views.cart),
    url(r'^orders/$', views.orders),
    url(r'^checkout/$', views.checkout),

    url(r'^get_role/$', views.get_role),
    url(r'^products/(?P<pid>\w+)/$', views.products),
    url(r'^addtocart/(?P<cid>\w+)/$', views.add_tocart),
    url(r'^remove/(?P<rid>\w+)/$', views.remove),
    url(r'^order_items/(?P<oid>\w+)/$', views.order_items),



    url(r'^(?P<username>\w+)/$', views.user_home),

    url(r'^', views.home),


]


