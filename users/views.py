from django.shortcuts import render

# Create your views here.
from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
#from models import Employee,Student
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response ,redirect
# Create your views here.
from django.contrib.auth.decorators import login_required

from django.http import HttpResponse, HttpResponseRedirect 
#from django.shortcuts import render_to_response ,redirect
from django.template import RequestContext

from django.contrib.auth.models import User
from django.contrib import auth
try:
    import json
except ImportError:
   from django.utils import simplejson as json
from django.core.serializers.json import DjangoJSONEncoder
from products.models import Categories,Item
from shopping.settings import SITE_ROOT,MEDIA_ROOT
from cart.models import TotalCart,CartItems,Order,OrderItems
import decimal



def home(request):
    """
        If user is logged in send him to user home else 
        stay on same page
    """

    user = request.user

    if user.username != '' or not user:

        user = User.objects.get(username=user.username)
        
        

        
        return HttpResponseRedirect("/"+str(user.username)+"/")

    else:
        return render (request,'index.html')

@login_required(login_url='/')
def user_home(request, username):
    """
        user home of various roles
    """
    data = {}
    print "user hoe"
    if request.user.username == username:
        user=None
        try:
            user = User.objects.get(username=username)
        except Exception as e:
            print e



        if user is not None:
            
            cat = Categories.objects.all()
            data['user'] = user
            data['cat'] = cat
            data['root'] = MEDIA_ROOT
            return render(request,'user_home.html', data)
    else:
        return HttpResponseRedirect("/"+request.user.username+"/")


def login(request):
    """
        login/authentication view
    """
    print "login"
    username = request.POST.get("username", "")
    password = request.POST.get("password", "")

    user = auth.authenticate(username=username, password=password)

    if user is not None:
        auth.login(request, user)

        

        return HttpResponseRedirect("/"+str(user.username)+"/")

    else:
        return HttpResponseRedirect("/invalid/")


def logout(request):
    """
        Logout the current user
    """
    user = request.user
    data = {}

    user_active = 0

    if user.username != '':

        user_active = 1
        logout_success = 0
        try:
            auth.logout(request)
            logout_success = 1
        except Exception as e:
            print e

        data['logout_success'] = logout_success
        data['user_active'] = user_active
    else:
        return HttpResponseRedirect("/")

    return render_to_response('users/logout.html', RequestContext(request, data))



def get_role(request):
    """
        Called on every page load, to display menu options and unread notifications count
    """ 
    data = {}

    if request.method == "POST":
        username = request.user.username
       
        try:
            cart = TotalCart.objects.get(user=request.user,status=0)
            count = cart.total
        except:
            count = 0

        data['count'] = count

        data['role'] = 'te'
        data['username'] = username

    return HttpResponse(json.dumps(data, cls=DjangoJSONEncoder).replace("'", "\'"), content_type='application/json')


def products(request,pid):
    data={}
    print pid
    username = request.user.username
    try:
        user = User.objects.get(username=username)
    except Exception as e:
        print e
    pro = Item.objects.filter(category__id=pid)

    if request.method == "POST":
        pro_id = request.POST.get("pro_id")
        print "pro_id is"
        print pro_id

    data['cat']=pro

    data['user']=user


    return render(request,'products.html',data)


def returns(request):

    data={}

    return render(request,'returns.html',data)

def offers(request):

    data={}

    return render(request,'offers.html',data)


def orders(request):

    data={}
    user = request.user
    cart = TotalCart.objects.filter(user=user,status=1)
    #cart_items = OrderItems.objects.filter(cart=cart)
    data['cart'] = cart

    return render(request,'orders.html',data)

def cart(request):

    data={}
    user = request.user
    try:
        cart = TotalCart.objects.get(user=user,status=0)
    except:
        cart = None
    if cart:

    
        cart_items = CartItems.objects.filter(cart=cart)
    else:
        cart_items = None
    data['cart'] = cart_items

    return render(request,'cart.html',data)

def add_tocart(request,cid):

    data={}
    print "cididi"
    user=request.user

    pro = Item.objects.get(id=cid)
    pro_id = str(pro.category.id)

    try:
        #import pdb;pdb.set_trace()
        cart = TotalCart.objects.get(user=user,status=0)
        cart.num_items += 1
        cart.total += decimal.Decimal(pro.price)
        cart.save()

    except:
        cart = TotalCart()
        cart.user = user
        cart.num_items = 1
        cart.total = decimal.Decimal(pro.price)
        cart.status = 0
        cart.save()

    try:
        cart_items = CartItems.objects.get(cart=cart,items=pro)
        cart_items.qty += 1
        cart_items.total += decimal.Decimal(pro.price)
        cart_items.save()

    except:
        cart_items = CartItems()
        cart_items.cart = cart
        cart_items.items = pro
        cart_items.qty = 1
        cart_items.total = decimal.Decimal(pro.price)
        cart_items.save()


    
 
    return redirect ('/products/'+pro_id+'/')


def remove(request,rid):

    data = {}
    user = request.user
    try:
        cart = TotalCart.objects.get(user = user,status=0)
    except:
        cart = None

    if cart:


        cart_items = CartItems.objects.get(id=rid,cart=cart)
        cart.num_items -= cart_items.qty
        cart.total -= cart_items.total
        cart.save()
        cart_items.delete()

    

    return redirect('/cart/')

def checkout(request):

    data={}
    user = request.user
    cart = TotalCart.objects.get(user=user,status=0)
    cart.status=1
    cart.save()

    return render(request,'checkout.html',data)

def order_items(request,oid):
    data={}

    user = request.user
    cart = TotalCart.objects.get(id=oid)
    cart_items = CartItems.objects.filter(cart=cart)
    data['cart'] = cart_items
    data['total'] = cart.total

    return render(request,'order_items.html',data)





















