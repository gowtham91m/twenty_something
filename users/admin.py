from django.contrib import admin

# Register your models here.
from users.models import Roles,Profile

# class AuthorAdmin(admin.ModelAdmin):
#     pass

admin.site.register(Roles)
admin.site.register(Profile)