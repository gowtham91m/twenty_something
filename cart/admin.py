from django.contrib import admin

# Register your models here.
from .models import TotalCart,CartItems,Order,OrderItems

admin.site.register(TotalCart)
admin.site.register(CartItems)
admin.site.register(Order)
admin.site.register(OrderItems)
