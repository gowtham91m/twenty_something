from __future__ import unicode_literals

from django.db import models

# Create your models here.
from django.db import models
from django.contrib.auth.models import User
from products.models import Categories,Item

class TotalCart(models.Model):
    user = models.ForeignKey(User)
    num_items = models.IntegerField()
    total = models.DecimalField(max_digits=10,decimal_places=2)
    status = models.IntegerField(default=-1)

    def __unicode__(self):

        return "%s %s %s" %(self.user,self.num_items,self.total)

class CartItems(models.Model):
    cart = models.ForeignKey(TotalCart)
    items = models.ForeignKey(Item)
    comments = models.CharField("Comments",max_length=255)
    qty = models.IntegerField()
    total = models.DecimalField(max_digits=10,decimal_places=2)

    def __unicode__(self):
        return "%s %s %s %s" %(self.cart,self.items,self.comments,self.qty)

class Order(models.Model):
    user = models.ForeignKey(User)
    num_items = models.IntegerField()
    total = models.DecimalField(max_digits=10,decimal_places=2)

    def __unicode__(self):

        return "%s %s %s" %(self.user,self.num_items,self.total)

class OrderItems(models.Model):
    cart = models.ForeignKey(TotalCart)
    items = models.ForeignKey(Item)
    comments = models.CharField("Comments",max_length=255)
    qty = models.IntegerField()
    total = models.DecimalField(max_digits=10,decimal_places=2)

    def __unicode__(self):
        return "%s %s %s %s" %(self.cart,self.items,self.comments,self.qty)