# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-29 07:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0002_auto_20161129_0701'),
    ]

    operations = [
        migrations.AddField(
            model_name='cartitems',
            name='total',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
