// Common functions

var incrementTime = 70;
var currentTime = 0;

var GSPKTimer =new (function(){ 
    this.resetStopwatch = function() {
        currentTime = 0;
        this.Timer.stop().once();
    };
    this.TimeTakenMS = function(){
        var ms_time = currentTime / 100; 
        return parseInt(ms_time);
    };
    this.StopTimer = function(){
        this.Timer.stop().once();
    };
});

function CallGSPKTimer(timerid) {
    $stopwatch = $("#"+timerid);
    var updateTimer = function(){
        $stopwatch.html(formatTime(currentTime));
        currentTime += incrementTime/10;
    };
    var initTimer = function(){
        GSPKTimer.Timer = $.timer(updateTimer, incrementTime, true);
    }
    $(initTimer);    
    
};

function CallGSPKTimerCT(timerid, current_time) {
    current_time = parseInt(current_time);
    console.log(current_time);
    var stopwatch = $("#"+timerid);
    var updateTimer = function(){
        stopwatch.html(formatTime(current_time));
        current_time += incrementTime/10;
    };
    var initTimer = function(){
        GSPKTimer.Timer = $.timer(updateTimer, incrementTime, true);
    }
    $(initTimer);    
    
};

function pad(number, length) {
    var str = '' + number;
    while (str.length < length) {str = '0' + str;}
    return str;
}

/*
function formatTime(time) {
    var min = parseInt(time / 6000),
        sec = parseInt(time / 100) - (min * 60),
        hundredths = pad(time - (sec * 100) - (min * 6000), 2);
    return (min > 0 ? pad(min, 2) : "00") + ":" + pad(sec, 2) + ":" + hundredths;
}
*/
function formatTime(time) {
    var hrs = parseInt(time/6000/60),
        min = parseInt(time / 6000) - (hrs * 60),
        sec = parseInt(time / 100) - (parseInt(time / 6000) * 60),
        hundredths = pad(time - (sec * 100) - (parseInt(time / 6000) * 6000), 2);

    return (hrs > 0 ? pad(hrs, 2) : "00") + ":" + (min > 0 ? pad(min, 2) : "00") + ":" + pad(sec, 2) + ":" + hundredths;
}
