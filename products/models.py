from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Categories(models.Model):
    
    name = models.CharField("name",max_length=255)
    shortcode = models.CharField("shortcode",max_length=255)
    comments = models.CharField("comments",max_length=255,blank=True,null=True)
    image = models.ImageField(upload_to="media/cat/images")


    def __unicode__(self):
        return "%s %s" %(self.name,self.shortcode)

class Item(models.Model):
    category = models.ForeignKey(Categories)
    name = models.CharField("name", max_length=255)
    shortcode = models.CharField("shortcode", max_length=255)
    comments = models.CharField("comments", max_length=255, blank=True, null=True)
    price = models.CharField("price",max_length=255)
    qty = models.CharField("Quantity",max_length=255,blank=True,null=True)
    image = models.ImageField(upload_to="media/pro/images")

    def __unicode__(self):
        return "%s %s %s" %(self.name,self.shortcode,self.category)